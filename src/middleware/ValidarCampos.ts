import { ErrorHandle400 } from './../helpers/ErrorHandler';
import { Request, Response, NextFunction} from 'express'
import { validationResult } from 'express-validator'

export const validarCampos = (req: Request, res: Response, next: NextFunction) => {
    const errores = validationResult(req)
    if(!errores.isEmpty()){
        return ErrorHandle400(res, errores.array())
    }

    next()
}