import { IBirth } from './interfaces/index';
import { Schema, model } from 'mongoose'

var birthSchema = new Schema({
    nombre: {
        type: String,
        required: true,
    },
    nacimiento: {
        type: Date,
        required:true
    }
}, {
    versionKey: false
})

export default model<IBirth>('Birth', birthSchema)