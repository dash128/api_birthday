import { Document } from 'mongoose'

export interface IBirth extends Document {
    nombre: String,
    nacimiento: Date
}