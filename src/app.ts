import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import birthRouter from './routers/birth.routes'

const app = express()

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())

app.use('/api/birth', birthRouter)
app.use('/', (req, res ) => res.send("la api esta en http://localhost:3000/api"))

export default app;