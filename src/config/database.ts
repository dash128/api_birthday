import { connect } from 'mongoose'

export const startConnection = async () => {
    
    try {
        const db = await connect('mongodb://localhost/db_birth')
        console.log('db runnig')
    } catch (error) {
        console.error(error)
    }
}