import { IBirth } from './../models/interfaces/index';
import { Request, Response } from 'express'
import { ErrorHandle404, ErrorHandle500 } from '../helpers/ErrorHandler';

import Birth from '../models/Birth'

export class BirthController {

    findAll = async (req: Request, res: Response) => {
        try {
            const births = await Birth.find() as (IBirth[])

            res.json(births)
        } catch (error: any) {
            return ErrorHandle500(res, error.message);
        }
    }

    findById = async (req: Request, res: Response) => {
        try {
            const id = req.params.id;

            const birth = await Birth.findById(id)
            if(!birth) return ErrorHandle404(res, 'Id Birth not found')

            res.json(birth)
        } catch (error: any) {
            return ErrorHandle500(res, error.message);
        }

    }
    
    create = async (req: Request, res: Response) => {
        try {
            const { nombre, nacimiento } = req.body;
            const birth = new Birth({nombre, nacimiento})
            await birth.save()
        
            res.json(birth)
        } catch (error: any) {
            return ErrorHandle500(res, error.message);
        }
    }
    
    update = async (req: Request, res: Response) => {
        try {
            const id = req.params.id
            const body = req.body as IBirth

            const birth = await Birth.findByIdAndUpdate(id, body, {new: true})
            if(!birth) return ErrorHandle404(res, 'Id Birth not found')
            
            res.json(birth)
        } catch (error: any) {
            let { message } = error
            return ErrorHandle500(res, message);
        }
    }
    
    delete = async (req: Request, res: Response) => {
        try {
            const id = req.params.id

            const birth = await Birth.findByIdAndDelete(id)
            if(!birth) return ErrorHandle404(res, 'Id Birth not found')
            
            res.json(birth)
        } catch (error: any) {
            return ErrorHandle500(res, error.message);
        }
    }
}