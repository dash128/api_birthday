import { Response } from 'express'
import { ValidationError } from 'express-validator'


export const ErrorHandle400 = (res: Response, mensaje: String | ValidationError[]) => {
    return res.status(400).json({
        mensaje
    })
}

export const ErrorHandle404 = (res: Response, mensaje: String) => {
    return res.status(404).json({
        mensaje
    })
}

export const ErrorHandle500 = (res: Response, mensaje: String) => {
    
    return res.status(500).json({
        mensaje
    })
}