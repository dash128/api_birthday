import { validarCampos } from './../middleware/ValidarCampos';
import { Router } from 'express'
import { check } from 'express-validator';
import { BirthController } from '../controllers/birthController'

class BirthRouter {
    public router: Router;
    private  birthController: BirthController;

    constructor(){
        this.router = Router()
        this.birthController = new BirthController
        this.routes()
    }

    routes = () => {
        this.router.get('/', this.birthController.findAll)
        this.router.get('/:id', this.birthController.findById)
        this.router.post('/', [
            check('nombre', 'nombre es obligatorio').not().isEmpty(),
            check('nacimiento', 'nacimiento es obligatorio').not().isDate(),
            validarCampos
        ], this.birthController.create)
        this.router.put('/:id', this.birthController.update)
        this.router.delete('/:id', this.birthController.delete)
    }
}
const birthRouter = new BirthRouter()
export default birthRouter.router