# About this BIRTHDAY APP
This app is a backend rest server with Express that consumes 5 main endpoints:
-   GET  -> findAll
-   GET  -> findById
-   POST -> create
-   PUT     ->  update
-   DELETE  ->  delete
## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

