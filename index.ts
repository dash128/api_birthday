import app from './src/app'
import { startConnection } from './src/config/database'

const port = 3000

startConnection()
app.listen(port)
console.log(`Server is running on port ${port}`)